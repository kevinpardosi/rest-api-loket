<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

include '../../../init.php';

include_once ROOT_DIR . '/config/config.php';

include_once ROOT_DIR . '/api/models/Ticket.php';

$dbclass = new config();
$connection = $dbclass->getConnection();

$ticket = new Ticket($connection);

$data = json_decode(file_get_contents("php://input"));

// make sure data is not empty
if (
        !empty($data->events_id) &&
        !empty($data->events_locations_id) &&
        !empty($data->tickets)
) {
    $ticket->events_id = $data->events_id;
    $ticket->events_locations_id = $data->events_locations_id;
    $ticket->data = $data->tickets;

    if ($ticket->create()) {
        // set response code - 201 created
        http_response_code(201);

        // tell the user
        echo json_encode($data);
    } else {
        // set response code - 503 service unavailable
        http_response_code(503);

        // tell the user
        echo json_encode(array("message" => "Unable to create event."));
    }
} else {

    // set response code - 400 bad request
    http_response_code(400);

    // tell the user
    echo json_encode(array("message" => "Unable to create event. Data is incomplete."));
}
?>