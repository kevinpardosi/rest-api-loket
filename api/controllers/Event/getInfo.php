<?php

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

include '../../init.php';

include_once ROOT_DIR . '/config/config.php';

include_once ROOT_DIR . '/api/models/Event.php';

$dbclass = new config();

$connection = $dbclass->getConnection();

$location = new Event($connection);

// query products
$stmt = $location->getInfo();
$num = $stmt->rowCount();

// check if more than 0 record found
if ($num > 0) {

    // retrieve our table contents
    while ($row = $stmt->fetchAll(PDO::FETCH_ASSOC)) {
        extract($row);

        $products_arr = [
            "id" => $row[0]['id'],
            "name" => $row[0]['name'],
            "start_at" => $row[0]['start_at'],
            "end_at" => $row[0]['end_at'],
            "location" => $row[0]['location'],
            "info" => $row[0]['info'],
            "tickets" => []
        ];

        foreach ($row as $dataObject) {
            $product_item = [
                "ticket_class" => $dataObject['ticket_class'],
                "ticket_price" => $dataObject['price'],
                "ticket_quota" => $dataObject['quota']
            ];

            array_push($products_arr["tickets"], $product_item);
        }
    }

    // set response code - 200 OK
    http_response_code(200);

    // show products data in json format
    echo json_encode($products_arr);
} else {

    // set response code - 404 Not found
    http_response_code(404);

    // tell the user no products found
    echo json_encode(
            array("message" => "No products found.")
    );
}
