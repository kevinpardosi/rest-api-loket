<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

include '../../init.php';

include_once ROOT_DIR . '/config/config.php';

include_once ROOT_DIR . '/api/models/Event.php';

$dbclass = new config();
$connection = $dbclass->getConnection();

$event = new Event($connection);

$data = json_decode(file_get_contents("php://input"));

// make sure data is not empty
if (
        !empty($data->locations_id) &&
        !empty($data->name) &&
        !empty($data->start_at) &&
        !empty($data->end_at)
) {
    $event->locations_id = $data->locations_id;
    $event->name = $data->name;
    $event->start_at = $data->start_at;
    $event->end_at = $data->end_at;
    $event->info = $data->info;

    if ($event->create()) {
        // set response code - 201 created
        http_response_code(201);

        // tell the user
        echo json_encode($data);
    } else {
        // set response code - 503 service unavailable
        http_response_code(503);

        // tell the user
        echo json_encode(array("message" => "Unable to create event."));
    }
} else {

    // set response code - 400 bad request
    http_response_code(400);

    // tell the user
    echo json_encode(array("message" => "Unable to create event. Data is incomplete."));
}
?>