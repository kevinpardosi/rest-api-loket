<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Location
 *
 * @author Kevin Pardosi <kevingatpardosi@gmail.com>
 */
class Location {

    //Connection instance
    private $connection;
    // table name
    private $table_name = "locations";
    // table columns
    public $id;
    public $name;

    public function __construct($connection) {
        $this->connection = $connection;
    }

    //C
    public function create() {
        
        $query = "INSERT INTO
                " . $this->table_name . "
            SET
                name=:name";

        $stmt = $this->connection->prepare($query);
        
        $this->name=htmlspecialchars(strip_tags($this->name));
        
        $stmt->bindParam(":name", $this->name);

        $stmt->execute();

        if ($stmt) {
            return true;
        }
        
        return false;
    }

    //R
    public function read() {
        $query = "SELECT * FROM " . $this->table_name;

        $stmt = $this->connection->prepare($query);

        $stmt->execute();

        return $stmt;
    }

    //U
    public function update() {
        
    }

    //D
    public function delete() {
        
    }

}
