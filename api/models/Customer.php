<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Customer
 *
 * @author Kevin Pardosi <kevingatpardosi@gmail.com>
 */
class Customer {
    //Connection instance
    private $connection;
    
    // table name
    private $table_name = "customers";

    // table columns
    public $id;
    public $name;
    public $email;
    
    public function __construct($connection){
        $this->connection = $connection;
    }

    public function read(){
        $query = "SELECT * FROM " . $this->table_name;

        $stmt = $this->connection->prepare($query);

        $stmt->execute();

        return $stmt;
    }   
}
