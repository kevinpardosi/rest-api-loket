<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Event
 *
 * @author Kevin Pardosi <kevingatpardosi@gmail.com>
 */
class Event {

    //Connection instance
    private $connection;
    // table name
    private $table_name = "events";
    // table columns
    public $id;
    public $location_id;
    public $name;
    public $start_at;
    public $end_at;
    public $info;

    public function __construct($connection) {
        $this->connection = $connection;
    }

    public function create() {
        $query = "INSERT INTO " . $this->table_name . "(locations_id, name, start_at, end_at, info) VALUES(:locations_id, :name, :start_at, :end_at, :info)";

        $stmt = $this->connection->prepare($query);

        $this->locations_id = htmlspecialchars(strip_tags($this->locations_id));
        $this->name = htmlspecialchars(strip_tags($this->name));
        $this->start_at = htmlspecialchars(strip_tags($this->start_at));
        $this->end_at = htmlspecialchars(strip_tags($this->end_at));

        $start = $this->start_at;
        $end = $this->end_at;

        $this->start_at = date("Y-m-d H:i:s", $start);
        $this->end_at = date("Y-m-d H:i:s", $end);

        $stmt->bindParam(":locations_id", $this->locations_id);
        $stmt->bindParam(":name", $this->name);
        $stmt->bindParam(":start_at", $this->start_at);
        $stmt->bindParam(":end_at", $this->end_at);
        $stmt->bindParam(":info", $this->info);

        $stmt->execute();

        if ($stmt) {
            return true;
        }

        return false;
    }

    public function getInfo() {
        $id = $_GET['id'];
        
        $query = "SELECT e.id, e.name, e.start_at, e.end_at, e.info, t.name AS 'ticket_class', t.price, t.quota, l.name AS 'location' 
            FROM `tickets` t 
            INNER JOIN (
                `locations` l INNER JOIN `events` e ON e.locations_id = l.id
            )
            ON t.events_id = e.id
            WHERE e.id = " . $id;

        $stmt = $this->connection->prepare($query);

        $stmt->execute();              

        return $stmt;
    }

}
