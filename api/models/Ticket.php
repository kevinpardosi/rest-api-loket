<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Location
 *
 * @author Kevin Pardosi <kevingatpardosi@gmail.com>
 */
class Ticket {

    //Connection instance
    private $connection;
    // table name
    private $table_name = "tickets";
    // table columns
    public $id;
    public $events_id;
    public $events_locations_id;
    public $name;
    public $price;
    public $quota;

    public function __construct($connection) {
        $this->connection = $connection;
    }

    public function create() {

        $this->events_id;
        $this->events_locations_id;

        $this->name = htmlspecialchars(strip_tags($this->name));

        foreach ($this->data as $ticketObject) {
            $query = "INSERT INTO " . $this->table_name . "(events_id, events_locations_id, name, price, quota) VALUES(:events_id, :events_locations_id, :ticket_class, :ticket_price, :ticket_quota)";

            $stmt = $this->connection->prepare($query);            
            
            $stmt->bindParam(":events_id", $this->events_id);
            $stmt->bindParam(":events_locations_id", $this->events_locations_id);
            $stmt->bindParam(":ticket_class", $ticketObject->ticket_class);
            $stmt->bindParam(":ticket_price", $ticketObject->ticket_price);
            $stmt->bindParam(":ticket_quota", $ticketObject->ticket_quota);

            $stmt->execute();
        }

        if ($stmt) {
            return true;
        }

        return false;
    }
}
