<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Database
 *
 * @author Kevin Pardosi <kevingatpardosi@gmail.com>
 */
class config {
    // specify your own database credentials
    private $host = "localhost";
    private $db_name = "loket";
    private $username = "root";
    private $password = "";
    private $port = 3307;
    public $connection;
 
    // get the database connection
    public function getConnection(){
 
        $this->connection = null;
 
        try{
            $this->connection = new PDO("mysql:host=" . $this->host . ";port=" . $this->port . ";dbname=" . $this->db_name, $this->username, $this->password);
            $this->connection->exec("set names utf8");
        }catch(PDOException $exception){
            echo "Connection error: " . $exception->getMessage();
        }
 
        return $this->connection;
    }
}
